﻿using BetterBLL;
using BetterDAL.Abstractions;
using BetterDAL.NoSQL;
using BetterDAL.SQL;
using System;

namespace BetterUI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What database you want to save data to?");
            Console.WriteLine("1. SQL");
            Console.WriteLine("2. NoSQL");
            string userDbChoice = Console.ReadLine();


            IDonutDatabase database;

            if (userDbChoice == "1")
            {
                database = new SQLDonutDatabase();
            }
            else if (userDbChoice == "2")
            {
                database = new NoSQLDonutDatabase();
            }
            else
            {
                Console.WriteLine("Wrong Choice");
                Console.ReadKey();
                return;
            }

            Donut donut = new Donut();
            DonutService service = new DonutService(database);
            service.SaveDonut(donut);

            Console.ReadKey();
        }
    }
}
