﻿using BetterDAL.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterBLL
{
    public class DonutService
    {
        private readonly IDonutDatabase _database;

        // Note that IDonutDatabase could not be replaced with SQLDonutDatabase or NoSQLDonutDatabase
        // due to missing project reference
        public DonutService(IDonutDatabase database)
        {
            _database = database;
        }

        public void SaveDonut(Donut donut)
        {
            _database.SaveDonut(donut);
        }
    }
}
