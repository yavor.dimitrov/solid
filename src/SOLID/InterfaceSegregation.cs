﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID
{
    #region Bad Example

    interface IWorker
    {
        void Work();
        void Eat();
    }

    class Person : IWorker
    {
        public void Eat() { /* Eating */ }

        public void Work() { /* Working */ }
    }

    class Robot : IWorker
    {
        public void Eat() { throw new NotImplementedException("Robots don't eat"); }

        public void Work() { /* Working */ }
    }

    class Manager
    {
        void Manage(IWorker worker)
        {
            worker.Work();
        }
    }

    #endregion

    #region Good Example

    interface IWorkable
    {
        void Work();
    }

    interface IFeedable
    {
        void Eat();
    }

    class AnotherPerson : IWorkable, IFeedable
    {
        public void Eat() { /* Eating */ }

        public void Work() { /* Working */ }
    }

    class AnotherRobot : IWorkable
    {
        public void Work() { /* Working */ }
    }

    class AnotherManager
    {
        void Manage(IWorkable worker)
        {
            worker.Work();
        }
    }

    #endregion
}
