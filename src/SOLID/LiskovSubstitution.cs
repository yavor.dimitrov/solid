﻿using SOLID.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID
{    
    #region Substitution Exapmle
    
    public class LiskovCalculator
    {


        // Before substitution
        public double CalculateArea(Shape[] shapes)
        {
            double totalArea = 0;
            foreach (var shape in shapes)
            {
                totalArea += shape.Area();
            }
            return totalArea;
        }

        // After substitution
        public double CalculateArea(Rectangle[] shapes)
        {
            double totalArea = 0;
            foreach (var shape in shapes)
            {
                totalArea += shape.Area();
            }
            return totalArea;
        }
    }

    #endregion


    #region Class hierarchy example

    // This is a bad example of a class hierarchy
    // Since Plane could not be substituted by PaperPlane
    // and expecting the application to work correctly 
    public class Plane
    {
        public void StartEngine() { }
        public void Fly() { }
    }

    public class JetPlane : Plane
    {
        public void DoSomeJetPlaneStuff() { }
    }

    public class PaperPlane : Plane
    {
        public void DoSomePaperyStuff() { }
    }

    // This is revised version of the class hierarchy
    public abstract class FlyingObject
    {
        public void Fly() { }
    }

    public class AirPlane : FlyingObject
    {
        public void StartEngine() { }
        public void DoSomeAirPlaneStuff() { }
    }

    public class Glider : FlyingObject
    {
        public void DoSomeGlideryStuff() { }
    }

    #endregion

}
