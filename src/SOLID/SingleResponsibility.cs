﻿using SOLID.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SOLID
{
    public class ExapmleOfACalculator
    {
        public double CalculateArea(Shape[] shapes)
        {
            double totalArea = 0;
            foreach (var shape in shapes)
            {
                totalArea += shape.Area();
            }
            return totalArea;
        }

        public double CalculateTotalPerimeters(Shape[] shapes)
        {
            double totalArea = 0;
            foreach (var shape in shapes)
            {
                totalArea += shape.Perimeter();
            }
            return totalArea;
        }


        // This should not be responsibility of the calculator class
        // It should be moved to anothe class which sole responsibility will be to send mails
        public void SendEmail(string mailAddress)
        {
            SmtpClient client = new SmtpClient();
            MailMessage message = new MailMessage(
                                    "myapp@test.test",
                                    mailAddress,
                                    "Your square is too big!",
                                    "Please fix your square.");
            client.Send(message);
        }
    }
}
