﻿using SOLID.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID
{
    // Lets imagine we have implemented shape library and a user is trying to use it in her project
    // We have implemented basic shapes and calculator but the user needs to implement additional shapes

    // User added shape
    public class Triangle : Shape
    {
        public double SideA { get; set; }

        public double SideB { get; set; }

        public double SideC { get; set; }

        public double HeightA { get; set; }

        public override double Area()
        {
            return SideA * HeightA / 2;
        }

        public override double Perimeter()
        {
            return SideA + SideB + SideC;
        }
    }

    // Calculates the combined area of multiple shapes
    // It violates the Open/Closed principle since
    // the user of the calculator is not able to extend the calculator functionality with another shape
    public class BadExampleOfACalculator
    {
        public double CalculateArea(Shape[] shapes)
        {
            double totalArea = 0;
            foreach (var shape in shapes)
            {
                if (shape is Rectangle)
                {
                    Rectangle rectangle = (Rectangle)shape;
                    totalArea += rectangle.Height * rectangle.Width;
                }
                else
                {
                    Circle circle = (Circle)shape;
                    totalArea += Math.PI * Math.Pow(circle.Radius, 2);
                }
            }

            return totalArea;
        }
    }


    // Calculates the combined area of multiple shapes
    // It does not viollate the Open/Closed principle since
    // the user could easily add new shapes by inheritance
    public class GoodExapmleOfACalculator
    {
        // This will work with a Triangle
        public double CalculateArea(Shape[] shapes)
        {
            double totalArea = 0;
            foreach (var shape in shapes)
            {
                totalArea += shape.Area();
            }
            return totalArea;
        }
    }


}
