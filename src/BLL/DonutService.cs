﻿using DAL;
using System;

namespace BLL
{
    public class DonutService
    {
        private DonutSQLDatabase _database;

        public DonutService(DonutSQLDatabase database)
        {
            _database = database;
        }

        public void SaveDonut(Donut donut)
        {
            _database.StoreDonutInSQLDatabase(donut);
        }
    }
}
