﻿using BLL;
using DAL;
using System;

namespace UI
{
    class Program
    {
        static void Main(string[] args)
        {
            DonutService donutService = new DonutService(new DonutSQLDatabase());

            donutService.SaveDonut(new Donut());
        }
    }
}
