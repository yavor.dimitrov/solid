﻿using BetterDAL.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterDAL.SQL
{
    public class SQLDonutDatabase : IDonutDatabase
    {
        public void SaveDonut(Donut donut)
        {
            Console.WriteLine($"Donut Saved to SQL database.");
        }
    }
}
