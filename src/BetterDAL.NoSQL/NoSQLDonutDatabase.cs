﻿using BetterDAL.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterDAL.NoSQL
{
    public class NoSQLDonutDatabase : IDonutDatabase
    {
        public void SaveDonut(Donut donut)
        {
            Console.WriteLine($"Donut Saved to No SQL database.");
            Console.WriteLine($"Triggered logic for new donuts.");
        }
    }
}
