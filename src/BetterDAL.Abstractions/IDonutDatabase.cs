﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterDAL.Abstractions
{
    public interface IDonutDatabase
    {
        void SaveDonut(Donut donut);
    }
}
